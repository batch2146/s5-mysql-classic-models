USE classic_models;

-- 1. 
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2. 
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. 
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4. 
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customerName FROM customers WHERE state IS NULL;

-- 6.
SELECT lastName, firstName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

-- 7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 8. 
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. 
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10.
SELECT DISTINCT country FROM customers;

-- 11. 
SELECT DISTINCT status FROM orders;

-- 12.
SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

-- 13.
SELECT employees.lastName, employees.firstName, offices.city FROM employees
    JOIN offices ON offices.officeCode = employees.officeCode WHERE offices.city = "Tokyo";

-- 14.
-- Two methods:
-- Using user define variables
SELECT employeeNumber INTO @var_employeeCodeLeslie FROM employees WHERE firstName = "Leslie" AND lastName = "Thompson";
SELECT customerName FROM customers WHERE salesRepEmployeeNumber = @var_employeeCodeLeslie;

-- Using JOIN method
SELECT customers.customerName FROM customers 
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";


-- 15. 
SELECT products.productName, customers.customerName
    FROM orders
        JOIN orderdetails 
            ON orders.orderNumber = orderdetails.orderNumber 
        JOIN products 
            ON orderdetails.productCode = products.productCode
        JOIN customers
            ON customers.customerNumber = orders.customerNumber WHERE customers.customerName = "Baane Mini Imports";

-- 16.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    JOIN offices ON customers.country = offices.country
GROUP BY employees.firstName, employees.lastName, customers.customerName, offices.country;

-- 17.
SELECT products.productName, products.quantityInStock FROM products
    JOIN productlines ON products.productLine = productlines.productLine WHERE productlines.productLine = "planes" AND products.quantityInStock < 1000;

-- 18.
SELECT customerName FROM customers WHERE phone LIKE "+81%";